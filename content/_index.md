---
title: home
date: 2022-08-26T01:48:52+01:00
---

This site was created with the goal of using the minimal Hugo code whilst still looking respecable. The build time is around one-fiftieth of a second on my aging PC.

There are 3 files:

- the config file, `hugo.toml` 
- the layout file, `layouts/_default/list.html` 
- a content file, `content/_index.md`

```bash
|-- content
|   `-- _index.md
|-- hugo.toml
|-- layouts
|   `-- _default
|       `-- list.html

3 directories, 3 files
```

Although the `list.html` file could go directly in the `/layouts/` directory this will cause Hugo to give a warning that there is no template available for creating taxonomy pages. When `list.html` is placed in the `/layouts/_default` subfolder Hugo can use this as a taxonomy template.

Newer versions of Hugo also create an empty `hugo_build.lock` file at every opportunity if one is not already present.


## The config file

The `hugo.toml` file can work with just one line:

```toml
title = "minimal"
```

However during the build Hugo's default behaviour will create folders for the taxonomies: `categories` and `tags`, each with it's own `index.html` and `index.xml` file. Hugo also generates an `index.xml` and `sitemap.xml` for the RSS feeds.

To turn all of these off and avoid generating 6 unnecessary files two more lines in the config file are needed:


```toml
disableKinds = ['RSS', 'sitemap']

[taxonomies]
```

Now, when the site is built, just one file is created, `index.html`.

I also decided to add a `baseURL` value so it would appear in the header using `{{ .Permalink }}`.

The full `hugo.toml` file now has 4 lines:

```toml
title = "minimal"
baseURL = "https://minimalhugo.vercel.app/"
disableKinds = ['RSS', 'sitemap']

[taxonomies]
```

## The HTML

The template file has just 9 lines of code between the `<body>` tags.

```HTML
<body>
    <main>
        <h1>{{ site.Title }}</h1>
        <div class="info">
            <time>{{ .Date.Format "Monday 2 Jan 2006" }} / {{ .Params.lastchange }}</time>
            <span>{{ .Permalink }}</span> <span>Hugo v.{{ hugo.Version }}</span></div>
        
        <section class="content">
            {{ .Content }}
        </section>
    </main>
</body>
```

## CSS

The CSS is embedded in the `<head>` section and contains the svg code for the background too.

To add an inline svg to the CSS directly you have to:

1. Change `"` to single quotes in the `<svg>`.
2. Change all instances of the hash sign, `#`, to `%23`.
3. Begin the url with `url("data:image/svg+xml;utf8, ...`
4. Begin and close the url brackets with a double quote: `url("...code ...")`


So...

```CSS
background-image: url("data:image/svg+xml;utf8,<svg>....</svg>")
```

More on this at [StackOverflow](https://stackoverflow.com/questions/10768451/inline-svg-in-css)

## Syntax highlighting

These are the default colours Hugo uses with one colour changed to blue in the CSS and the background made a semi-transparent black.

## Fonts

**Inter Tight** is used for the body and headings. **Azeret Mono** is used for code blocks. Both are linked to [Google fonts](https://fonts.google.com/) though can be found at many other websites.

Flexible font sizing and padding sizing by [Utopia.fyi](https://utopia.fyi).

## Background image

This was generated at [svgWaves.in](https://svgwaves.in/).


## Lighthouse scores

First go with Google lighthouse gave me scores of:

- 100 Perfomance
- 100 Accessability
- 100 Best Practices
- 91 SEO

I had forgotten to put in a description meta tag.

## Deploying on Vercel

First time I've used Vercel for a long time. Everything seemed super easy but the build failed because they failed to detect a framework. I thought the `hugo.toml` file might have given it away but apprently not. I'd even put the `HUGO_VERSION` into the environment variables but that didn't help.

So I had to write `hugo` as the build command in then redeploy. Finding the option to redeploy took about 20 mins!! It was hidden away under the statcked dots of the projects Deployment page. 

The full build time took 3s but that included installing the latest Hugo version.

The `hugo` build command took only 18ms.
